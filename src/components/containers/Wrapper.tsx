import React from "react";

const Wrapper = ({ children }: { children: any }) => {
  return (
    <div style={{ justifyContent: "center" , width:'100%',alignItems:'center'}}>
      {children}
    </div>
  );
};

export default Wrapper;
