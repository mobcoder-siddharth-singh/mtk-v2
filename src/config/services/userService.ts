import axios from "axios";
import Api from "../constants/Api";

axios.defaults.headers.common["accept"] = "application/json";
axios.defaults.headers.common["content-type"] = "multipart/form-data";

export const getFoodListService = () => axios.get(Api.GET);
