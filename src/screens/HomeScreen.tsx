import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
  Box,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Typography,
} from "@mui/material";
import { useState, useEffect } from "react";
import { Loader, Wrapper } from "../components";
import { getFoodListService } from "../config/services/userService";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";

import "../index.css";

const HomeScreen = (): JSX.Element => {
  const [foodsList, setFoodsList] = useState<any[]>();
  const [isLoading, setIsLoading] = useState<boolean>(false);

  useEffect(() => {
    getFoodListService()
      .then((res) => handleData(res))
      .catch((error) => console.log("error---", error));
  }, []);

  const handleData = (data: any) => {
    console.log("the data -----", data?.data);
    setFoodsList(data?.data?.scrap_data);
    setIsLoading(false);
  };

  const getSlides = (item: any,Object:any) => {
    return Object.values(item)[0].map((item: any) => (
      <Accordion sx={{ background: "none" }} elevation={0}>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel1a-content"
          id="panel1a-header"
        >
          <Typography> {Object.keys(item)[0]}</Typography>
        </AccordionSummary>
        <AccordionDetails>
          {Object.values(item).length > 0 ? (
            getGramSlides(item,Object)
          ) : (
            <Typography>No Data !</Typography>
          )}
        </AccordionDetails>
      </Accordion>
    ));
  };
  const getGramSlides = (item: any,Object:any) => {
    return Object.values(item)[0].map((item: any) => (
      <Accordion sx={{ background: "none" }} elevation={0}>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel1a-content"
          id="panel1a-header"
        >
          <Typography> {Object.keys(item)[0]}</Typography>
        </AccordionSummary>
        <AccordionDetails>
          {Object.values(item).length > 0 ? (
            getfinalInfo(item,Object)
          ) : (
            <Typography>No Data !</Typography>
          )}
        </AccordionDetails>
      </Accordion>
    ));
  };
  const getfinalInfo = (item: any,Object:any) => {
    return (
      <>
        <TableRow sx={{ background: "none" }}>
          <TableCell align="left">Nutrient</TableCell>
          <TableCell align="left">Amount</TableCell>
        </TableRow>

        <TableRow>
          <TableCell align="left">Protein</TableCell>
          <TableCell align="left">

            {Object.values(item)[0][0].Protein}
          </TableCell>
        </TableRow>
        <TableRow>
          <TableCell align="left">Calories</TableCell>
          <TableCell align="left">
            {Object.values(item)[0][0].Calories}
          </TableCell>
        </TableRow>
      </>
    );
  };
  return (
    <Wrapper>
      <Box sx={{ backgroundColor: "#f0fffa" }}>
        <Table stickyHeader>
          <TableHead>
            <TableCell align="left">S No.</TableCell>
            <TableCell align="left">Name</TableCell>
          </TableHead>
          {!isLoading ? (
            <TableBody>
              {foodsList
                ? foodsList.map((item: any, index: number) => {
                    return (
                      <TableRow key={item.fdcId} hover>
                        <TableCell align="left">{index + 1}</TableCell>
                        <TableCell align="left">
                          <Accordion sx={{ background: "none" }} elevation={0}>
                            <AccordionSummary
                              expandIcon={<ExpandMoreIcon />}
                              aria-controls="panel1a-content"
                              id="panel1a-header"
                            >
                              <Typography> {Object.keys(item)[0]}</Typography>
                            </AccordionSummary>
                            <AccordionDetails>
                              {Object.values(item).length > 0 ? (
                                getSlides(item,Object)
                              ) : (
                                <Typography>No Data !</Typography>
                              )}
                            </AccordionDetails>
                          </Accordion>
                        </TableCell>
                      </TableRow>
                    );
                  })
                : null}
            </TableBody>
          ) : null}
        </Table>
        {foodsList?.length === 0 ? (
          <Typography
            variant="h6"
            component="h1"
            color={"#adadad"}
            marginTop={"20%"}
            textAlign={"center"}
          >
            No Data Found !
          </Typography>
        ) : isLoading ? (
          <Box textAlign={"center"} marginTop="20%">
            <Loader width="2rem" height="2rem" borderWidth=".4rem" />
          </Box>
        ) : null}
      </Box>
    </Wrapper>
  );
};

export default HomeScreen;
